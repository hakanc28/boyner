﻿using System;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Threading;

namespace ConfigReader
{
    public class ConfigurationReader
    {
        private string _applicationName;
        private string _connectionString;
        private int _refreshTimerIntervalInMs;

        public Boolean isRunning = false;

        public List<ConfigModel> data = new List<ConfigModel>();

        public ConfigurationReader(string applicationName, string connectionString, int refreshTimerIntervalInMs)
        {
            _applicationName = applicationName;
            _connectionString = connectionString;
            _refreshTimerIntervalInMs = refreshTimerIntervalInMs;

            isRunning = true;

            Thread thread = new Thread(new ThreadStart(ReadDB));
            thread.Start();
        }

        public void ReadDB()
        {
            while (isRunning)
            {
                SqlConnection cnn = new SqlConnection(_connectionString);

                cnn.Open();

                SqlCommand cmd = new SqlCommand("select Name,Type,Value,IsActive,ApplicationName from Configuration where IsActive = 1 and ApplicationName = '" + _applicationName + "'", cnn);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    ConfigModel c = new ConfigModel();

                    c.Name = reader.GetString(0);
                    c.Type = reader.GetString(1);
                    c.Value = reader.GetString(2);
                    c.IsActive = reader.GetBoolean(3);
                    c.ApplicationName = reader.GetString(4);

                    data.Add(c);
                }

                cnn.Close();

                Thread.Sleep(_refreshTimerIntervalInMs);
            }
        }

        public T GetValue<T>(string key)
        {
            return (T)Convert.ChangeType(key, typeof(T));
        }
    }

    public class ConfigModel
    {
        public string Name { get; set; }
        public string Type { get; set; }
        public string Value { get; set; }
        public Boolean IsActive { get; set; }
        public string ApplicationName { get; set; }
    }
}
