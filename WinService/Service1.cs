﻿using ConfigReader;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinService
{
    public partial class Service1 : ServiceBase
    {
        ConfigurationReader _configurationReader = null;

        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            _configurationReader = new ConfigurationReader("SERVICE-A", "Server=DESKTOP-ON90V99;Database=BOYNER;Trusted_Connection=True;", 5000);
        }

        protected override void OnStop()
        {
            _configurationReader.isRunning = false;
            Application.Exit();
        }

        public void StartService()
        {
            OnStart(null);
        }

        public void StopService()
        {
            OnStop();
        }
    }
}
