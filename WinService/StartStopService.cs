﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WinService
{
    public partial class StartStopService : Form
    {
        private Service1 s = null;

        public StartStopService()
        {
            InitializeComponent();

            s = new Service1();

            button1.Enabled = true;
            button2.Enabled = false;

            label1.Visible = false;
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            s.StartService();

            button1.Enabled = false;
            button2.Enabled = true;

            label1.Visible = true;
            label1.Text = "Running";
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            s.StopService();

            button1.Enabled = true;
            button2.Enabled = false;

            label1.Visible = true;
            label1.Text = "Stopped";
        }
    }
}
