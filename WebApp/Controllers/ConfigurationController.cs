﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Net;
using System.Web;
using System.Web.Mvc;
using WebApp.Models;

namespace WebApp.Controllers
{
    public class ConfigurationController : Controller
    {
        private BOYNEREntities db = new BOYNEREntities();

        // GET: Configuration
        public async Task<ActionResult> Index()
        {
            return View(await db.Configurations.ToListAsync());
        }

        // GET: Configuration/Create
        public ActionResult Create()
        {
            ViewBag.Type = new SelectList(new List<string>() { "Int", "Double", "String", "Boolean" });
            return View();
        }

        // POST: Configuration/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "ID,Name,Type,Value,IsActive,ApplicationName")] Configuration configuration)
        {
            if (ModelState.IsValid)
            {
                db.Configurations.Add(configuration);
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }

            return View(configuration);
        }

        // GET: Configuration/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Configuration configuration = await db.Configurations.FindAsync(id);
            if (configuration == null)
            {
                return HttpNotFound();
            }
            ViewBag.Type = new SelectList(new List<string>() { "Int", "Double", "String", "Boolean" }, configuration.Type);
            return View(configuration);
        }

        // POST: Configuration/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "ID,Name,Type,Value,IsActive,ApplicationName")] Configuration configuration)
        {
            if (ModelState.IsValid)
            {
                db.Entry(configuration).State = EntityState.Modified;
                await db.SaveChangesAsync();
                return RedirectToAction("Index");
            }
            return View(configuration);
        }
        
        public async Task<ActionResult> ChangeActivity(int id)
        {
            Configuration configuration = await db.Configurations.FindAsync(id);

            if (configuration.IsActive == true) configuration.IsActive = false;
            else configuration.IsActive = true;

            await db.SaveChangesAsync();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
